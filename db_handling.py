# This module handles database requests.

import hashlib
import os
import apsw
from apsw import Error
import sys

# The algorithm used to hash passwords
def passwordHashing(input, salt):
    return hashlib.pbkdf2_hmac('sha256', input.encode('utf-8'), salt, 100000)

# Returns list of users from the user db, matching the given user_id
def getUserdata(user_id):
    stmt = ('SELECT username, password, salt, token FROM users WHERE username GLOB ?')
    c = conn.execute(stmt, [(user_id)])
    return c.fetchall()

# Set a secure password with salt and hash and store in SQL db, if username is available
def addNewUser(user_id, password):
    rows = getUserdata(user_id)
    if len(rows) == 0:
        salt = os.urandom(32)
        key = passwordHashing(password, salt)
        stmt = ('INSERT INTO users (username, password, salt) values (?,?,?)')
        try:
            conn.execute(stmt, [user_id, key, salt])
        except Error as e:
            print(e)
            conn.rollback()
            sys.exit(1)

# Check if input matches name and password stored in the SQL db
def check_password(user_id, inputPass):
    userdata = getUserdata(user_id)
    if len(userdata) == 0:
        return False
    if userdata[0][0] == user_id and userdata[0][1] == passwordHashing(inputPass, userdata[0][2]):
        return True
    return False

# Returns the unique integer ID of the user. Usernames are already unique, but ehh
def getUserInt(user_id):
    stmt = ('SELECT id FROM users WHERE username = ?')
    c = conn.execute(stmt, [(user_id)])
    c = c.fetchall()[0][0]
    return c

# Adds a new chat with a list of users to the database
def addNewChat(chatName, listOfUsers):
    try:
        stmt = ('INSERT INTO chatnames (name) values (?) RETURNING chat')
        chatID = conn.execute(stmt, [(chatName)]).fetchall()[0][0]
        stmt = (f'INSERT INTO userinchats (chat, user) values ({chatID}, ?)')
        listOfUserInts = [[getUserInt(x)] for x in listOfUsers]
        conn.executemany(stmt, (listOfUserInts))
        return chatID
    except Error as e:
        print('oopsie')
        print(e)
        sys.exit(1)

# Returns a list of users in a given chat
def getUsersInChat(chatID):
    stmt = ('''SELECT users.username FROM users INNER JOIN userinchats ON 
    users.id = userinchats.user INNER JOIN chatnames ON chatnames.chat = userinchats.chat 
    WHERE chatnames.chat = ? ''')
    c = conn.execute(stmt, [chatID])
    list = [x[0] for x in c]
    return  list

# Searches through messages in chat IF requester is part of chat
def getMessagesFromChat(chatID, user, searchword='*'):
    if not user in getUsersInChat(chatID):
        return [f'User not in chat with this ID']
    if searchword !='*':
        searchword = '*'+searchword+'*'
    stmt = ('SELECT * FROM messages WHERE chatID GLOB ? AND message GLOB ?')
    c = conn.execute(stmt, [chatID, searchword])
    c = c.fetchall()
    return c

# Returns a list of chats that the given user is a part of. ! Parameter is integer ID, not username
def getMyChats(user_int_id):
    stmt = ('''SELECT chatnames.chat, chatnames.name FROM chatnames INNER JOIN userinchats 
            ON userinchats.chat = chatnames.chat WHERE userinchats.user GLOB ? ''')
    c = conn.execute(stmt, [(user_int_id)])
    c = c.fetchall()
    return c

# Returns a list of chats, each chat having a list of all messages that match search
def searchMyChats(user, searchword='*'):
    if searchword != '*':
        searchword = '*'+searchword+'*'
    listOfChats = getMyChats(getUserInt(user))
    toReturn = []
    for x in listOfChats:
        chatID = x[0]
        next = getMessagesFromChat(chatID, user, searchword)
        if bool(next):
            toReturn.append(next)
    return toReturn

# Sends a message in the given chat IF sender is part of chat
def sendMessage(fromuser, chatID, message):
    if not fromuser in getUsersInChat(chatID):
        return [f'User not in chat with this ID']
    stmt = ('INSERT INTO messages (chatID, sender, message) values (?,?,?)')
    conn.execute(stmt, [chatID, fromuser, message])
    return 'OK'

try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        chatID integer,
        sender TEXT NOT NULL,
        message TEXT NOT NULL,
        timestamp DATETIME DEFAULT CURRENT_TIMESTAMP);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    # Add table with users
    c.execute('''CREATE TABLE IF NOT EXISTS users (
        id integer PRIMARY KEY, 
        username TEXT NOT NULL,
        password BLOB,
        salt BLOB,
        token BLOB);''')
    # Add table of user-ids and chat-ids
    c.execute('''CREATE TABLE IF NOT EXISTS userinchats (
        id integer PRIMARY KEY,
        chat integer,
        user integer);''')
    # Add table of chat-ids and chat names
    c.execute('''CREATE TABLE IF NOT EXISTS chatnames (
        chat integer PRIMARY KEY,
        name TEXT NOT NULL);''')
except Error as e:
    print(e)
    sys.exit(1)
