# This module is for various resources (for example icons) the server may want to fetch

from flask import send_from_directory, abort, make_response
from app import app
from pygments.formatters import HtmlFormatter

cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')

# An easter egg
@app.get('/coffee/')
def nocoffee():
    abort(418)

# Another easter egg
@app.route('/coffee/', methods=['POST', 'PUT'])
def gotcoffee():
    return "Thanks!"

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp