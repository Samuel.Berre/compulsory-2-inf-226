# Part 2A - implementation

The first thing I did was finishing the login server project, which included adding a logout-button, patching up the SQL injection vulnerability, hashing and salting passwords, and moving them into the SQL database, instead of them just being stored in a dictionary.

I've decided to use the included SQLite to persist information.

Things that have been refactored:
   - Databases have their own module
   - Added a seperate module for various resources, like icons and URLs with limited functionality (/coffee)
   - Messaging now has its own module
---------------------------------------------------------
# Part 2B - Documentation

This application is a messaging app similar to Discord, where you talk in chats with other people.
It has these functions available through its UI:
   - Logging in and out
   - Making new chats with list of users
   - Displaying all messages that user is authorized to read (messages in chat that user is part of)
   - Searching for key words in specific chat, or in all messages
   - Seeing list of chats that user is part of

I also made more functions in backend, but didn't have enough time to implement them in frondend D:

## How to run:

   - Run 'flask run' in terminal in the root folder of the project
   - Connect to http://localhost:5000/
   - You can log in with, for example, username: 'alice', password: 'password123'
   - The UI is a mess, and uses the same text boxes for multiple purposes:
   - To send a message, you need to specify a chatID (number), write a message, and click send.
   - To make a new chat, write the name of chat in "search", and the users you want to add (don't add yourself)
   in "message", seperated by a space, then click "Create new chat"
   - To see the chatID of the new chat, tap "My chats", and see the number next to the name of the chat.
   - If you want to search in all chats, remove the chatID.

## Security

These are some of its security features:
   - Protects against SQL injection by using prepared statements
   - Hashes and salts passwords before storing them in database
   - Only allows users who are logged in to acces their messages
   - Uses HTML escaping to fight cross site scripting attacks
   - Refactored methods into their own modules, which makes code easier to analyze
   - Uses CSRF protection with flask_wtf (tokens)

I have tested SQL injections, confidentiality of messages and HTML escaping and they all seem to work fine.
I'm a little uncertain about my implementation of CSRF tokens, as I don't use HTML forms the way I've seen it
implemented in the official documents.
There are many types of software that can be used to check 

I didn't get around to creating the messaging API.
I also wanted to add is_safe_url(), but never got around to doing it

## Threat model:
Possible attackers would be really any user of the application, someone who wants to harm the user or the server.

Since the website communicates with a server that could have personal information on it, there is a risk of loss of
confidentiality. If an attacker gets hold of a user session, there could also be loss of integrity.

Some threats are:
   - DDOS-attacks, which I have not implemented any defense for, and would be bad for the service's availability.
   - Brute forcing into accounts. I have not implemented any restrictions on bad passwords, or two-factor authentication.
      This would be a threat to confidentiality and integrity, as personal information could be stolen, or someone could
      pretend to be someone they're not.
   - 

Great attack vectors include the login, for example caused by weak credentials, a leak, or a lack of encryption.
Cross site request forgery may also be a greater issue because of the way /login currently redirects to an unchecked URL.
Since this is a rushed project, misconfiguration and other forgotten vulnerabilities are also main attack vectors. By trying common
security flaws, an attacker could (very likely) find a loophole I forgot about, or didn't manage to implement.
   
For the access control model, I've tried to give users as few privileges as possible, by, for example, abstracting away any SQL queries,
hiding how backend operates, require login for most methods and making limited options, keeping experimental functions away from frontend.

I do not log all activity to the database, so if there were malicious activity, I would not have any way of checking where
things went wrong, or what was done, which would feel very dumb when the day comes that the service is attacked.
