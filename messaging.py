# This module handles recieving requests from HTML and sending them to the db_handling, where they become queries
# Then it gets the return from db_handling and sends it back to HTML

from threading import currentThread
from flask import request
from flask_login import login_required, current_user
from app import pygmentize, app
from markupsafe import escape
from json import dumps
import db_handling
from apsw import Error

# Searches through messages in db that the current user has access to
@app.route('/search', methods=['POST', 'GET'])
@login_required
def search():
    query = request.args.get('q') or request.form.get('q') or '*'
    chatID = request.args.get('chatID') or request.args.get('chatID')
    result = f"Search input: {pygmentize(query)}\n"
    try:
        if chatID == '':
            dbquery = sum(db_handling.searchMyChats(current_user.id, query), [])
        else:
            dbquery = db_handling.getMessagesFromChat(chatID, current_user.id, query)
        result = result + 'Result:\n'
        for row in dbquery:
            result = f'{result}    {escape(dumps(row))}\n'
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

# Sends message (adds to db), with given parameters
@app.route('/send', methods=['POST'])
@login_required
def send():
    try:
        message = request.args.get('message') or request.args.get('message')
        chatID = request.args.get('chatID') or request.args.get('chatID')
        print(message)
        if not message or not chatID:
            return f'ERROR: missing chatID, sender or message'
        result = f"Sender: {pygmentize(current_user.id)}, Message: {pygmentize(message)}\n"
        try:
            chatID = int(chatID)
        except ValueError as e:
            return f'{result}ERROR: {e}'
        dbquery = db_handling.sendMessage(current_user.id, chatID, message)
        return f'{result} + {escape(dbquery)}'
    except Error as e:
        return f'{result}ERROR: {e}'

# Returns the chats that current user is authorized to read/send to
@app.route('/mychats', methods=['POST','GET'])
@login_required
def getChats():
    try:
        query = db_handling.getMyChats(db_handling.getUserInt(current_user.id))
        result = ""
        for row in query:
            result = f'{result}    {escape(dumps(row))}\n'
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

# Creates a new chat with the current user and a given list of other users
@app.route('/newchat', methods=['POST'])
@login_required
def newchat():
    try:
        name = request.args.get('chatname') or request.args.get('chatname')
        users = request.args.get('users') or request.args.get('users')
        if not name or not users:
            return f'Missing name or userlist'
        print(users)
        users = users.split()
        users.append(current_user.id)
        print(users)
        dbquery = db_handling.addNewChat(name, users)
        return f'New chat has chatID {dbquery}'
    except Error as e:
        return f'{dbquery}ERROR: {e}'

# Haven't found a use for this yet
@app.route('/announcements', methods=['POST', 'GET'])
@login_required
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = db_handling.conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}